#include <stdio.h>
#include <stdlib.h>

int main()
{
    int x,y ;
    printf("Enter a Positive integer =");
    scanf("%d",&x);
    printf("Factors of %d are: ",x);
    y=1;
    while(y<=x)
    {
        if(x%y==0) printf("%d",y);
        y++;
    }
    printf("\n");
    return 0;
}
