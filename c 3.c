#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a,b,x=0;
    printf("Enter a number to reverse =");
    scanf("%d",&a);
    while (a!=0)
    {
        b= a % 10;
        x= x * 10 + b;
        a= a/10;
    }
    printf("Reversed number =%d",x);
    return 0;
}
